package entity;

//import java.lang.reflect.Type;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Robot {

    private List<Detail> details;
    private String name;
    private int detailsCounter = 0;
    private static final int maxDetails = 4;
    private static final int minDetails = 1;
    private Scientist scientist;

    public Robot(String name, Scientist scientist) {
        this.name = name;
        details = new ArrayList<>();
        this.scientist=scientist;
    }

    public Robot(String name) {
        this.name = name;
        details = new ArrayList<>();
    }

    public synchronized boolean takeDetailsFromLandfill(Landfill landfill) {
        try {
            int randomCount = minDetails + (int) (Math.random() * maxDetails);
            while (detailsCounter < randomCount) {
                if (landfill.getAllDetailsExistingInLandfill().size() != 0) {
                    Detail d = getRandomDetail(landfill);
                    details.add(d);
                    System.out.println(this.name + " takes: " + d.getType());
                    landfill.getAllDetailsExistingInLandfill().remove(d);
                    detailsCounter++;
                } else {
                    System.out.println(name + " can't take more");
                    return false;
                }
            }
            detailsCounter = 0;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    public synchronized List<Detail> getAllDetails() {
        List<Detail> list = new ArrayList<>();
        try {
            if (detailsCounter >= minDetails) {
                for (Detail d : details) {
                    detailsCounter--;
                    System.out.println(details.size() + " details is taken from robot: " + Thread.currentThread().getName());
                    details.remove(d);
                    list.add(d);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public synchronized void giveDetailsToScientist() {
        scientist.setDetailList(details);
    }

    private Detail getRandomDetail(Landfill l) {
        Random random = new Random();
        return l.getAllDetailsExistingInLandfill().get(random.nextInt(l.getAllDetailsExistingInLandfill().size()));
    }

}
