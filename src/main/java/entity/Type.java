package entity;

import java.util.ArrayList;
import java.util.List;

public enum Type {
    HEAD,
    BODY,
    LeftArm,
    RightArm,
    LeftLeg,
    RightLeg,
    CPU,
    RAM,
    HDD;

    Type getType(){
        return this.getType();
    }

    public static List<Type> getAllTypes(){
        List<Type> list = new ArrayList<>();
        list.add(Type.HEAD);
        list.add(Type.BODY);
        list.add(Type.LeftArm);
        list.add(Type.RightArm);
        list.add(Type.LeftLeg);
        list.add(Type.RightLeg);
        list.add(Type.HDD);
        list.add(Type.RAM);
        list.add(Type.CPU);

        return list;
    }
}
