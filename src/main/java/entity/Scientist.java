package entity;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Scientist {

    private String name;
    private List<Detail> detailList;

    public Scientist(String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public List<Detail> getDetailList() {
        return detailList;
    }

    public void setDetailList(List<Detail> detailList) {
        this.detailList = detailList;
    }
}
