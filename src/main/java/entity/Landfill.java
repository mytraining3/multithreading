package entity;

//import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class Landfill {

    List<Detail> details;
    private static final int maxDetails = 4;
    private static final int minDetails = 1;
    private static final int existingDetailsAtFirstNight = 20;
    private int detailsCounter = 0;

    public Landfill() {
        details = new ArrayList<>();
    }

    public synchronized boolean add(Detail detail){
        try{
            if(detailsCounter < maxDetails) {  //поменять на условие от 1 до 4х деталей
                notifyAll();
                details.add(detail);
                System.out.print(detail.getType() + "; ");
                detailsCounter++;
            } else {
                wait();
                return false;
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        detailsCounter = 0;
        return true;
    }

    public List<Detail> getAllDetailsExistingInLandfill(){
        return details; 
    }

}
