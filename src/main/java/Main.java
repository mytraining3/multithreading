import entity.Landfill;
import entity.Robot;
import entity.Scientist;
import task.DetailGenerator;
import task.RobotCollectDetails;
import task.ScientistCompetition;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {

    public static void main(String[] args) throws InterruptedException {

        Landfill landfill = new Landfill();
        DetailGenerator generator = new DetailGenerator(landfill);

        Scientist scientist1 = new Scientist("scientist1");
        Scientist scientist2 = new Scientist("scientist2");


        Robot robot1 = new Robot("Robot'1", scientist1);
        Robot robot2 = new Robot("Robot'2", scientist2);

        RobotCollectDetails collectDetails1 = new RobotCollectDetails(landfill, robot1);
        RobotCollectDetails collectDetails2 = new RobotCollectDetails(landfill, robot2);

        ScientistCompetition s1 = new ScientistCompetition(scientist1);
        ScientistCompetition s2 = new ScientistCompetition(scientist2);

        for (int i = 1; i <= 100; i++){
            generator.run();
            collectDetails1.run();
            collectDetails2.run();
            Thread.sleep(1000);//засыпает на ночь
        }

        ExecutorService service = Executors.newFixedThreadPool(2);
        service.execute(s1);
        service.execute(s2);
        service.shutdown();
    }
}
