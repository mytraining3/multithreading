package task;

import entity.Landfill;
import entity.Robot;

public class RobotCollectDetails extends Thread {

    private Landfill landfill;
    private Robot robot;

    public RobotCollectDetails(Landfill landfill, Robot robot){
        this.landfill = landfill;
        this.robot = robot;
    }

    @Override
    public void run() {
        synchronized (robot) {
            robot.takeDetailsFromLandfill(landfill);
            robot.giveDetailsToScientist();
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
