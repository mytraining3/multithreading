package task;

import entity.Detail;
import entity.Scientist;
import entity.Type;

import java.util.*;

public class ScientistCompetition implements Runnable {

    private List<Detail> allDetails;
    private Scientist scientist;


    public ScientistCompetition(Scientist scientist) {
        allDetails = new ArrayList<>();
        this.scientist = scientist;
    }

    @Override
    public void run() {
        allDetails = scientist.getDetailList();
        howMuchRobotsConstructed();
    }

    public int howMuchRobotsConstructed() {

        Map<Type, Integer> map = new HashMap();
        for (Detail d : allDetails) {
            if (map.containsKey(d.getType()))
                map.put(d.getType(), map.get(d.getType()) + 1);
            else
                map.put(d.getType(), 1);
        }

        if (map.size() == Type.getAllTypes().size()) { //проверяем, что все нужные детали для целого робота есть
            System.out.println(scientist.getName() + " has : " + map + " and collected " + Collections.min(map.values()) +" full robots");
            return Collections.min(map.values());
        } else {
            System.out.println(scientist.getName() + " has : " + map + " and it's not enough to collect full robot");
        }
        return 0;
    }

}
