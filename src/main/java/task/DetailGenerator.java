package task;

import entity.Type;
import entity.Detail;
import entity.Landfill;

import java.util.Random;

public class DetailGenerator implements Runnable {

    private Landfill landfill;
    private int detailsInLandfill = 20;
    private int detailsCounter = 4;

    public DetailGenerator(Landfill landfill) {
        this.landfill = landfill;
    }

    @Override
    public void run() {
        int count = 0;
        System.out.print("Landfill get from factory : ");
        while (count < detailsCounter) {
            Thread.currentThread().setName(" Generator details");
            count++;
            landfill.add(new Detail(getRandomType()));
        }
        System.out.println();
    }

    private Type getRandomType() {
        Random random = new Random();
        return Type.values()[random.nextInt(Type.values().length)];
    }

}
